package com.momentum.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MomentumServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MomentumServicesApplication.class, args);
	}
	
}

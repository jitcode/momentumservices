package com.momentum.app.repository.security;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.momentum.app.models.dto.UsuarioVista;
import com.momentum.app.models.entity.security.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long>{

	Usuario findByCorreo(String correo);
	
	@Query(value = "select u from Usuario u")
	List<UsuarioVista> encuentraTodos();
	
	UsuarioVista findUsuarioVistaById(Long id);

}

package com.momentum.app.repository.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.momentum.app.models.entity.security.Menu;

@Repository
public interface MenuRepository extends CrudRepository<Menu, Long>{

}

package com.momentum.app.repository.security;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.momentum.app.models.entity.security.Rol;

@Repository
public interface RolRepository extends CrudRepository<Rol, Long>{

}

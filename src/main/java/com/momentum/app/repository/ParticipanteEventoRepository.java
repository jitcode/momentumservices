package com.momentum.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.momentum.app.models.entity.Evento;
import com.momentum.app.models.entity.Participante;
import com.momentum.app.models.entity.ParticipanteEvento;

@Repository
public interface ParticipanteEventoRepository extends CrudRepository<ParticipanteEvento, Long>{
	
	List<ParticipanteEvento> findByEvento(Evento evento);

	ParticipanteEvento findByEventoAndParticipante(Evento evento, Participante participante);

}

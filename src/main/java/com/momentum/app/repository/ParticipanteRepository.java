package com.momentum.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.momentum.app.models.entity.Participante;

@Repository
public interface ParticipanteRepository extends CrudRepository<Participante, Long>{

	@Query(value="select * from participante where UPPER(CONCAT(nombres, apellido_paterno, apellido_materno)) like %:nombres%", nativeQuery = true) 
	List<Participante> findByNombreContainsIgnoreCase(@Param("nombres") String nombres);

	List<Participante> findByParticipanteEnrolador(Participante participanteEnrolador);

}

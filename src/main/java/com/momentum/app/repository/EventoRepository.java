package com.momentum.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.momentum.app.models.entity.Evento;
import com.momentum.app.models.entity.TipoEvento;

@Repository
public interface EventoRepository extends CrudRepository<Evento, Long>{

	List<Evento> findByTipoEvento(TipoEvento tipoEvento);

	@Query(value="select * from evento where fecha_fin >= DATE(now()) order by fecha_inicio asc limit 5", nativeQuery = true)
	List<Evento> findProximosEventos();
	
	@Query(value="select e.* from evento as e\r\n" + 
					"inner join tipo_evento as te on te.id = e.id\r\n" + 
					"where UPPER(CONCAT(te.descripcion, ' Invictus ', e.generacion)) like %:descripcion%", nativeQuery = true)
	List<Evento> findLikeDescripcion(@Param("descripcion") String descripcion);

}

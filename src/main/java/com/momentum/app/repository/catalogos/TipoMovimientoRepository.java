package com.momentum.app.repository.catalogos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.momentum.app.models.entity.catalogos.TipoMovimiento;

@Repository
public interface TipoMovimientoRepository extends CrudRepository<TipoMovimiento, Long>{

	List<TipoMovimiento> findAllByOrderByIdAsc();
	
}

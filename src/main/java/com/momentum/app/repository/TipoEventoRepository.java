package com.momentum.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.momentum.app.models.entity.TipoEvento;

@Repository
public interface TipoEventoRepository extends CrudRepository<TipoEvento, Long>{
	
	List<TipoEvento> findAllByOrderByIdAsc();

}

package com.momentum.app.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.momentum.app.models.entity.Evento;
import com.momentum.app.models.entity.Movimientos;
import com.momentum.app.models.entity.Participante;

@Repository
public interface MovimientoRepository extends CrudRepository<Movimientos, Long>{

	List<Movimientos> findAllByOrderByIdAsc();

	List<Movimientos> findByParticipanteOrderByIdAsc(Participante participante);

	List<Movimientos> findByEventoOrderByIdAsc(Evento evento);

	List<Movimientos> findByParticipanteAndEventoOrderByIdAsc(Participante participante, Evento evento);
	
	@Query(value ="select * "
			+ "from movimientos "
			+ "where fecha >= :fechaInicio and fecha <= :fechaFin" ,
			nativeQuery = true)
	List<Movimientos> findByFechas(@Param("fechaInicio") Date fechaInicio, @Param("fechaFin") Date fechaFin);

}

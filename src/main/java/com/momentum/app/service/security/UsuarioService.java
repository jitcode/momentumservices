package com.momentum.app.service.security;

import java.util.Date;
import java.util.List;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.app.configuration.security.PasswordUtils;
import com.momentum.app.exception.MomentumException;
import com.momentum.app.models.dto.ActualizarUsuarioRequestModel;
import com.momentum.app.models.dto.UsuarioChangePassword;
import com.momentum.app.models.dto.UsuarioModel;
import com.momentum.app.models.dto.UsuarioVista;
import com.momentum.app.models.entity.security.Usuario;
import com.momentum.app.repository.security.UsuarioRepository;
import com.momentum.app.util.ValidationsUtil;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioRepository repository;
	
	public List<UsuarioVista> findAll(){
		return this.repository.encuentraTodos();
	}	

	public UsuarioModel save(Usuario usuario) {
		Usuario usuarioSaved;
		String salt;

		if (!validarUsuario(usuario)) {
			throw new MomentumException("El usuario es incorrecto.");
		}

		usuarioSaved = this.repository.findByCorreo(usuario.getCorreo());

		if (usuarioSaved != null) {
			throw new MomentumException("El correo no existe");
		}

		// Generate Salt. The generated value can be stored in DB.
		salt = PasswordUtils.getSalt(30);

		usuario.setPassword(PasswordUtils.generateSecurePassword(usuario.getPassword(), salt));
		usuario.setSalt(salt);
		usuario.setIntentosFallidos(0);
		usuario.setFechaAlta(new Date());
		usuario.setFechaActualizacionPassword(new Date());
		usuario.setEstatus("Activo");

		try {
			usuario = this.repository.save(usuario);
		} catch(ConstraintViolationException ex) {
			throw new MomentumException(ValidationsUtil.getConstraintMessage(ex));
		}
		return new UsuarioModel(usuario);
	}

	public Boolean validarUsuario(Usuario usuario) {
		
		if(usuario.getCorreo()== null || usuario.getCorreo().isEmpty()) {
			throw new MomentumException("El correo se encuentra vacio.");
		}
		
		if(usuario.getPassword() == null || usuario.getPassword().isEmpty()) {
			throw new MomentumException("El password se encuentra vacio.");
		}

		return true;
	}


	public UsuarioModel actualizar(Long idUsuario, ActualizarUsuarioRequestModel usuario) {
		UsuarioModel usuarioReturn = new UsuarioModel();
		
		//Guardamos solo lo que se puede cambiar
		Usuario usuarioToSaved = this.repository.findById(idUsuario).get();
		
		usuarioToSaved.setCorreo(usuario.getCorreo());
		usuarioToSaved.setApellidoPaterno(usuario.getApellidoPaterno());
		usuarioToSaved.setApellidoMaterno(usuario.getApellidoMaterno());
		usuarioToSaved.setNombres(usuario.getNombres());
		
		//TODO Revisar que actualice correctamente
		usuarioToSaved = this.repository.save(usuarioToSaved);
		
		return usuarioReturn;
	}

	public UsuarioVista findById(Long idUsuario) {
		return this.repository.findUsuarioVistaById(idUsuario);
	}

	public void deleteById(Long idUsuario) {
		this.repository.deleteById(idUsuario);
	}

	public UsuarioVista cambiarPassword(Long id, UsuarioChangePassword usuario) {
		Usuario usuarioSaved = this.repository.findById(id).get();
		
		if(usuarioSaved == null) {
			throw new MomentumException("Hubo un error al actualizar el usuario.");
		}
		
		if(!PasswordUtils.verifyUserPassword(usuario.getPasswordAnterior(), usuarioSaved.getPassword(), usuarioSaved.getSalt())) {
			throw new MomentumException("El password anterior no coincide.");
		}
		
		if(!usuario.getPasswordNuevo().equals(usuario.getConfirmacionPasswordNuevo())) {
			throw new MomentumException("El password nuevo y su confirmación debe de coincidir.");
		}
		
		usuarioSaved.setPassword(PasswordUtils.generateSecurePassword(usuario.getPasswordNuevo(), usuarioSaved.getSalt()));
		
		usuarioSaved = this.repository.save(usuarioSaved);
		
		UsuarioVista usuarioVista = (UsuarioVista) usuarioSaved;
		
		return usuarioVista;
	}	


}

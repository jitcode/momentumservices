package com.momentum.app.service.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.app.configuration.security.JWTTokenUtil;
import com.momentum.app.configuration.security.PasswordUtils;
import com.momentum.app.exception.MomentumException;
import com.momentum.app.models.dto.UsuarioModel;
import com.momentum.app.models.entity.security.Usuario;
import com.momentum.app.repository.security.UsuarioRepository;

import lombok.extern.java.Log;

@Log
@Service
public class LoginService {
	
	@Autowired
	private UsuarioRepository repository;
	

	public UsuarioModel login(Usuario usuario) {
		Boolean isCorrecto = true;
		Usuario usuarioSaved;
		UsuarioModel usuarioReturn = new UsuarioModel();
		
		log.info("AUTENTICANDO USUARIO");

		usuarioSaved = this.repository.findByCorreo(usuario.getCorreo());

		if (usuarioSaved == null) {
			isCorrecto = false;
		}
		
		if (!isCorrecto) {
			throw new MomentumException("La contraseña o el usuario son incorrectos.");
		}

		isCorrecto = PasswordUtils.verifyUserPassword(usuario.getPassword(), usuarioSaved.getPassword(),
				usuarioSaved.getSalt());

		if (!isCorrecto) {
			throw new MomentumException("La contraseña o el usuario son incorrectos.");
		}
		
		usuarioReturn.setCorreo(usuario.getCorreo());
		usuarioReturn.setToken(JWTTokenUtil.getJWTToken(usuario.getCorreo()));
		usuarioReturn.setRol(usuarioSaved.getRol());
		usuarioReturn.setNombre(usuarioSaved.getNombres());
		
		return usuarioReturn;
	}


}

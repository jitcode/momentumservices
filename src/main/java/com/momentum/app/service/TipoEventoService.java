package com.momentum.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.app.exception.MomentumException;
import com.momentum.app.models.entity.Evento;
import com.momentum.app.models.entity.TipoEvento;
import com.momentum.app.repository.TipoEventoRepository;

@Service
public class TipoEventoService {
	
	@Autowired
	private TipoEventoRepository repository;
	
	@Autowired
	private EventoService eventoService;	
	
	public TipoEvento save(TipoEvento tipoEvento) {
		if(tipoEvento.getDescripcion() == null || tipoEvento.getDescripcion().isEmpty()) {
			throw new MomentumException("Favor de capturar una descripcion."); 
		}
		
		return this.repository.save(tipoEvento);
	}
	
	public List<TipoEvento> findAll(){
		return this.repository.findAllByOrderByIdAsc();
	}

	public TipoEvento eliminar(Long id) {
		
		TipoEvento tipoEvento = this.repository.findById(id).get();
		
		if(tipoEvento!=null) {
			
			List<Evento> listaEventos = this.eventoService.findByTipoEvento(tipoEvento);
			
			if(listaEventos!=null && !listaEventos.isEmpty()) {
				throw new MomentumException("No se puede eliminar el tipo de evento " + tipoEvento.getDescripcion() + " ya que existen eventos de este tipo."); 
			}
			
		}
		
		this.repository.delete(tipoEvento);
		return tipoEvento;
	}


}

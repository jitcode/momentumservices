package com.momentum.app.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.app.exception.MomentumException;
import com.momentum.app.models.entity.Participante;
import com.momentum.app.repository.ParticipanteRepository;

import lombok.extern.java.Log;

@Log
@Service
public class ParticipanteService {
	
	@Autowired
	private ParticipanteRepository repository;
	
	public Participante save(Participante participante) {
		
		if(participante.getNombres() == null || participante.getNombres().isEmpty()) {
			throw new MomentumException("El campo nombre esta vacio.");
		}
		
		if( participante.getNumeroTelefono() == null || ( participante.getNumeroTelefono()!=null && participante.getNumeroTelefono().isEmpty())) {
			throw new MomentumException("Favor de capturar un número de telefono.");			
		}
		
		if( participante.getId()!=null && participante.getParticipanteEnrolador() != null && participante.getParticipanteEnrolador().getId()== participante.getId() ) {
			throw new MomentumException("El participante no puede ser invitado de el mismo.");			
		}
		
		if( participante.getId()==null) {
			participante.setFechaAlta(new Date()); 
		}
		
		participante.setNombres(participante.getNombres().trim());
		
		if(participante.getApellidoPaterno()!=null)
			participante.setApellidoPaterno(participante.getApellidoPaterno().trim());
		
		if(participante.getApellidoMaterno()!=null)
			participante.setApellidoMaterno(participante.getApellidoMaterno().trim());
		
		return this.repository.save(participante);
	}
	
	public List<Participante> findAll() {
		return (List<Participante>) this.repository.findAll();
	}

	public Participante findById(Long id) {
		Optional<Participante> participante = this.repository.findById(id);
		
		if(participante==null) { 
			throw new MomentumException("No se encontro información del participante.");
		}
		
		return participante.get();    
	}

	public void deleteById(Long id) {
		this.repository.deleteById(id);
	}

	public Participante actualizar(Long idParticipante, Participante participante) {
		participante = this.repository.save(participante);
		return participante;
	}
	
	public List<Participante> findByNombres(String nombres) {
		log.info("Buscando : " + nombres);
		String filterValue = nombres.trim().toUpperCase().replace(" ", "%");
		
		return this.repository.findByNombreContainsIgnoreCase(filterValue);
	}

	public List<Participante> findByParticipanteEnrolador(Long idParticipante) {
		Participante participanteEnrolador = this.findById(idParticipante);
		
		return this.repository.findByParticipanteEnrolador(participanteEnrolador);
	}


}

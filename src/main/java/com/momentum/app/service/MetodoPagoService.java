package com.momentum.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.app.exception.MomentumException;
import com.momentum.app.models.entity.MetodoPago;
import com.momentum.app.repository.MetodoPagoRepository;

@Service
public class MetodoPagoService {
	
	@Autowired
	private MetodoPagoRepository repository;
	
	public List<MetodoPago> findAll(){
		return this.repository.findAllByOrderByIdAsc();
	}

	public MetodoPago save(MetodoPago metodoPago) {
		return this.repository.save(metodoPago);
	}

	public MetodoPago eliminar(Long id) {		
		MetodoPago metodoPago = null;
		
		try {
			metodoPago = this.repository.findById(id).get();
		} catch(Exception e) {
			throw new MomentumException("No se pudo completar esta operación.");
		}
		
		if(metodoPago != null) {
			this.repository.delete(metodoPago);
		}
		
		return metodoPago;
	}


}

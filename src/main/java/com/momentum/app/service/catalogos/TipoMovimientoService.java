package com.momentum.app.service.catalogos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.app.models.entity.catalogos.TipoMovimiento;
import com.momentum.app.repository.catalogos.TipoMovimientoRepository;

@Service
public class TipoMovimientoService {
	
	@Autowired
	private TipoMovimientoRepository repository;
	
	public List<TipoMovimiento> findAll(){
		return this.repository.findAllByOrderByIdAsc();
	}


}

package com.momentum.app.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.app.exception.MomentumException;
import com.momentum.app.models.entity.Evento;
import com.momentum.app.models.entity.Movimientos;
import com.momentum.app.models.entity.Participante;
import com.momentum.app.repository.MovimientoRepository;

@Service
public class MovimientosService {
	
	@Autowired
	private MovimientoRepository repository;
	
	@Autowired
	private ParticipanteService participanteService;
	
	@Autowired
	private EventoService eventoService;
	
	public List<Movimientos> findAll(){
		return this.repository.findAllByOrderByIdAsc();
	}
	
	public List<Movimientos> findByEventoAndParticipante(Long idEvento, Long idParticipante) {
		Participante participante = this.participanteService.findById(idParticipante);
		
		if(participante==null) {
			throw new MomentumException("El participante no se encuentra en el sistema.");
		}
		
		Evento evento = this.eventoService.findById(idEvento);
		
		if(evento == null) {
			throw new MomentumException("El evento no se encuentra en el sistema.");
		}
		
		return this.repository.findByParticipanteAndEventoOrderByIdAsc(participante, evento);
	}
	
	public List<Movimientos> findByParticipante(Long id) {
		Participante participante = this.participanteService.findById(id);
		
		if(participante==null) {
			throw new MomentumException("El participante no se encuentra en el sistema.");
		}
		
		return this.repository.findByParticipanteOrderByIdAsc(participante);
	}
	
	public List<Movimientos> findByEvento(Long id) {
		Evento evento = this.eventoService.findById(id);
		
		if(evento == null) {
			throw new MomentumException("El evento no se encuentra en el sistema.");
		}
		
		return this.repository.findByEventoOrderByIdAsc(evento);
	}
	
	public List<Movimientos> findBetweenFechas(Date fechaInicio, Date fechaFin) {
		return this.repository.findByFechas(fechaInicio, fechaFin);
	}

	public Movimientos save(Movimientos movimiento) {
		return this.repository.save(movimiento);
	}

	public Movimientos eliminar(Long id) {		
		Movimientos movimiento = null;
		
		try {
			movimiento = this.repository.findById(id).get();
		} catch(Exception e) {
			throw new MomentumException("No se pudo completar esta operación.");
		}
		
		if(movimiento != null) {
			this.repository.delete(movimiento);
		}
		
		return movimiento;
	}


}

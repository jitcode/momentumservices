package com.momentum.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.momentum.app.exception.MomentumException;
import com.momentum.app.models.entity.Evento;
import com.momentum.app.models.entity.Participante;
import com.momentum.app.models.entity.ParticipanteEvento;
import com.momentum.app.models.entity.TipoEvento;
import com.momentum.app.repository.EventoRepository;
import com.momentum.app.repository.ParticipanteEventoRepository;
import com.momentum.app.repository.ParticipanteRepository;

@Service
public class EventoService {
	
	@Autowired
	private EventoRepository repository;
	
	@Autowired
	private ParticipanteEventoRepository participanteEventoRepository;
	
	@Autowired
	private ParticipanteRepository participanteRepository;
	
	@Autowired
	private ParticipanteService participanteService;
	
	public Evento save(Evento evento) {
		
		if(evento.getTipoEvento() == null) {
			throw new MomentumException("El campo descripción esta vacio.");
		}
		
		if(evento.getGeneracion() == null || evento.getGeneracion().isEmpty()) {
			throw new MomentumException("El campo generacion esta vacio.");			
		}
		
		if(evento.getFechaInicio() == null || evento.getFechaFin() == null ||
				evento.getFechaFin().before(evento.getFechaInicio())) {
			throw new MomentumException("Las fechas son incorrectas.");			
		}
		
		return this.repository.save(evento);
	}
	
	public List<Evento> findByTipoEvento(TipoEvento tipoEvento){
		return this.repository.findByTipoEvento(tipoEvento);
	}
	
	public List<Evento> findAll() {
		return (List<Evento>) this.repository.findAll();
	}

	public Evento findById(Long id) {
		Optional<Evento> evento = this.repository.findById(id);
		
		if(evento==null || !evento.isPresent()) { 
			throw new MomentumException("No se encontro información del evento.");
		}
		
		return evento.get();    
	}

	public void deleteById(Long id) {
		List<ParticipanteEvento> listaParticipantes = this.findParticipantes(id);
		
		if(listaParticipantes != null && !listaParticipantes.isEmpty()) {
			throw new MomentumException("El evento no puede ser eliminado ya que contiene participantes.");
		}
		
		this.repository.deleteById(id);
	}

	public List<ParticipanteEvento> findParticipantes(Long id) {
		Evento evento = this.findById(id);
		
		List<ParticipanteEvento> listaParticipanteEvento = this.participanteEventoRepository.findByEvento(evento);
						
		return listaParticipanteEvento;
	}

	public ParticipanteEvento findParticipante(Long id, Long idParticipante) {
		Evento evento = this.findById(id);
		Participante participante = this.participanteRepository.findById(idParticipante).get();
		
		
		return this.participanteEventoRepository.findByEventoAndParticipante(evento, participante);
	}

	public ParticipanteEvento addParticipante(Long id, ParticipanteEvento informacion) {
		Evento evento = this.findById(id);
		
		//En Caso de que no exista el participante, lo agregamos
		if(informacion.getParticipante().getId()==null) {
			informacion.setParticipante(this.participanteService.save(informacion.getParticipante()));
		}		
		
		ParticipanteEvento participanteEvento = this.participanteEventoRepository.findByEventoAndParticipante(evento, informacion.getParticipante());
		
		if(participanteEvento != null ) {
			throw new MomentumException("El participante ya se encuentra dado de alta para el evento " + evento.getTipoEvento().getDescripcion());
		} else {
			participanteEvento = new ParticipanteEvento();
			
			participanteEvento.setAsistio(false);
			participanteEvento.setCosto(informacion.getCosto());
			participanteEvento.setGraduado(false);
			participanteEvento.setEvento(evento);
			participanteEvento.setParticipante(informacion.getParticipante());
		}
		
		return this.participanteEventoRepository.save(participanteEvento);
	}	
	
	public ParticipanteEvento actualizarParticipante(Long id, ParticipanteEvento informacion) {		
		//actualizamos al participante
		informacion.setParticipante(this.participanteService.save(informacion.getParticipante()));
		
		return this.participanteEventoRepository.save(informacion);
	}
	
	public ParticipanteEvento deleteParticipante(Long id, Long idParticipante) {
		ParticipanteEvento participanteEvento = this.findParticipante(id, idParticipante);
		
		if(participanteEvento!=null) {
			this.participanteEventoRepository.delete(participanteEvento);
		} else {
			throw new MomentumException("El participante no se encuentra dado de alta para el evento");			
		}
		
		return participanteEvento;
	}

	public List<Evento> findProximos() {
		return this.repository.findProximosEventos();
	}

	public ParticipanteEvento asistioParticipante(Long id, Long idParticipante) {
		Evento evento = this.findById(id);
		Participante participante = this.participanteService.findById(idParticipante);
		
		ParticipanteEvento participanteEvento = this.participanteEventoRepository.findByEventoAndParticipante(evento, participante);
		
		participanteEvento.setAsistio(!participanteEvento.getAsistio());		
		
		return this.participanteEventoRepository.save(participanteEvento);
	}

	public ParticipanteEvento graduadoParticipante(Long id, Long idParticipante) {
		Evento evento = this.findById(id);
		Participante participante = this.participanteService.findById(idParticipante);
		
		ParticipanteEvento participanteEvento = this.participanteEventoRepository.findByEventoAndParticipante(evento, participante);
		
		participanteEvento.setGraduado(!participanteEvento.getGraduado());		
		
		return this.participanteEventoRepository.save(participanteEvento);
	}
	
	public List<Evento> findLikeDescripcion(String descripcion){
		return this.repository.findLikeDescripcion(descripcion.toUpperCase());
	}


}

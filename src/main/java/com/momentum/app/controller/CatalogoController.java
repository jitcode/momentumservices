package com.momentum.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.momentum.app.models.entity.catalogos.TipoMovimiento;
import com.momentum.app.service.catalogos.TipoMovimientoService;

@RestController
@RequestMapping(value = "/catalogos")
public class CatalogoController {
	
	@Autowired
	private TipoMovimientoService tipoMovimientoService;
	
	@GetMapping(value="tipomovimiento")
	private List<TipoMovimiento> findAllTipoMovimiento() {
		return this.tipoMovimientoService.findAll();
	}
	
}

package com.momentum.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.momentum.app.models.dto.ActualizarUsuarioRequestModel;
import com.momentum.app.models.dto.UsuarioChangePassword;
import com.momentum.app.models.dto.UsuarioModel;
import com.momentum.app.models.dto.UsuarioVista;
import com.momentum.app.models.entity.security.Usuario;
import com.momentum.app.service.security.UsuarioService;

@RestController
@RequestMapping(value = "/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService service;
	
	@PostMapping
	public UsuarioModel save(@RequestBody Usuario usuario) {
		return this.service.save(usuario);
	}
	
	@GetMapping
	public List<UsuarioVista> findAll(){
		return this.service.findAll();
	}
	
	@PutMapping("/{id}")
	public UsuarioModel actualizar(@PathVariable("id") Long idUsuario, @RequestBody ActualizarUsuarioRequestModel usuario) {
		return this.service.actualizar(idUsuario, usuario);
	}
	
	@GetMapping("/{id}")
	public UsuarioVista findById(@PathVariable("id") Long idUsuario) {
		return this.service.findById(idUsuario);
	}
	
	@DeleteMapping("/{id}")
	public void eliminarById(@PathVariable("id") Long idUsuario) {
		this.service.deleteById(idUsuario);
	}
	
	@PatchMapping("/{id}/password")
	public UsuarioVista cambiarPassword(@PathVariable("id") Long idUsuario, @RequestBody UsuarioChangePassword usuario) {
		return this.service.cambiarPassword(idUsuario, usuario);
	}
}
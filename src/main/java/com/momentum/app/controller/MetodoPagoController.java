package com.momentum.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.momentum.app.models.entity.MetodoPago;
import com.momentum.app.service.MetodoPagoService;

@RestController
@RequestMapping(value = "/metodopago")
public class MetodoPagoController {
	
	@Autowired
	private MetodoPagoService service;
	
	@PostMapping
	private MetodoPago save(@RequestBody MetodoPago metodoPago) {
		return this.service.save(metodoPago);
	}
	
	@PutMapping("/{id}")
	private MetodoPago actualizar(@PathVariable("id") Long id, @RequestBody MetodoPago metodoPago) {
		return this.service.save(metodoPago);
	}
	
	@GetMapping
	private List<MetodoPago> findAll() {
		return this.service.findAll();
	}
	
	@DeleteMapping("/{id}")
	private MetodoPago eliminar(@PathVariable("id") Long id) {
		return this.service.eliminar(id);
	}

}

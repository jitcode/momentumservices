package com.momentum.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.momentum.app.models.entity.Evento;
import com.momentum.app.models.entity.ParticipanteEvento;
import com.momentum.app.service.EventoService;

@RestController
@RequestMapping(value = "/evento")
public class EventoController {
	
	@Autowired
	private EventoService service;
	
	@PostMapping
	private Evento save(@RequestBody Evento evento) {
		return this.service.save(evento);
	}
	
	@GetMapping
	private List<Evento> findAll(){
		return this.service.findAll();
	}
	
	@PutMapping("/{id}")
	private Evento actualizar(@PathVariable("id") Long id, @RequestBody Evento evento) {
		return this.service.save(evento);
	}
	
	@GetMapping("/{id}")
	private Evento findById(@PathVariable("id") Long id) {
		return this.service.findById(id);
	}
	
	@GetMapping("/proximos")
	private List<Evento> findProximos() {
		return this.service.findProximos();
	}
	
	@GetMapping("/search/{descripcion}")
	private List<Evento> findLikeDescripcion( @PathVariable("descripcion") String descripcion) {
		return this.service.findLikeDescripcion(descripcion);
	}
	
	@DeleteMapping("/{id}")
	private void deleteById(@PathVariable("id") Long id) {
		this.service.deleteById(id);
	}
	
	@GetMapping("/{id}/participante")
	private List<ParticipanteEvento> findParticipantes(@PathVariable("id") Long id) {
		return this.service.findParticipantes(id);
	}
	
	@GetMapping("/{id}/participante/{idParticipante}")
	private ParticipanteEvento findParticipante(@PathVariable("id") Long id, @PathVariable("idParticipante") Long idParticipante) {
		return this.service.findParticipante(id, idParticipante);
	}
	
	@DeleteMapping("/{id}/participante/{idParticipante}")
	private ParticipanteEvento borrarParticipante(@PathVariable("id") Long id, @PathVariable("idParticipante") Long idParticipante) {
		return this.service.deleteParticipante(id, idParticipante);
	}
	
	@PostMapping("/{id}/participante")
	private ParticipanteEvento addParticipante(@PathVariable("id") Long id, @RequestBody ParticipanteEvento informacion) {
		return this.service.addParticipante(id, informacion);
	}
	
	@PutMapping("/{id}/participante/{idParticipante}")
	private ParticipanteEvento actualizarParticipante(@PathVariable("id") Long id, @PathVariable("idParticipante") Long idParticipante, @RequestBody ParticipanteEvento informacion) {
		return this.service.actualizarParticipante(id, informacion);
	}
	
	@PatchMapping("/{id}/participante/{idParticipante}/asistio")
	private ParticipanteEvento asistioParticipante(@PathVariable("id") Long id, @PathVariable("idParticipante") Long idParticipante) {
		return this.service.asistioParticipante(id, idParticipante);
	}
	
	@PatchMapping("/{id}/participante/{idParticipante}/graduado")
	private ParticipanteEvento graduadoParticipante(@PathVariable("id") Long id, @PathVariable("idParticipante") Long idParticipante) {
		return this.service.graduadoParticipante(id, idParticipante);
	}


}

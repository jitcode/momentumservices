package com.momentum.app.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.momentum.app.models.entity.Movimientos;
import com.momentum.app.service.MovimientosService;

@RestController
@RequestMapping(value = "/movimientos")
public class MovimientosController {
	
	@Autowired
	private MovimientosService service;
	
	@PostMapping
	private Movimientos save(@RequestBody Movimientos tipoEvento) {
		return this.service.save(tipoEvento);
	}
	
	@PutMapping("/{id}")
	private Movimientos actualizar(@PathVariable("id") Long id, @RequestBody Movimientos movimiento) {
		return this.service.save(movimiento);
	}
	
	@GetMapping
	private List<Movimientos> findAll() {
		return this.service.findAll();
	}
	
	@DeleteMapping("/{id}")
	private Movimientos eliminar(@PathVariable("id") Long id) {
		return this.service.eliminar(id);
	}
	
	@GetMapping("/participante/{id}")
	private List<Movimientos> findByParticipante(@PathVariable("id") Long id) {
		return this.service.findByParticipante(id);
	}
	
	@GetMapping("/evento/{id}")
	private List<Movimientos> findByEvento(@PathVariable("id") Long id) {
		return this.service.findByEvento(id);
	}
	
	@GetMapping("/evento/{id}/participante/{idParticipante}")
	private List<Movimientos> findByEventoAndParticipante(@PathVariable("id") Long id, @PathVariable("idParticipante") Long idParticipante) {
		return this.service.findByEventoAndParticipante(id, idParticipante);
	}
	
	@GetMapping("/fechas")
	private List<Movimientos> findByFechas(Date fechaInicio, Date fechaFin) {
		return this.service.findBetweenFechas(fechaInicio, fechaFin);
	}

}

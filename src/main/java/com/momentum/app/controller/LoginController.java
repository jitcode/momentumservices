package com.momentum.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.momentum.app.models.dto.UsuarioModel;
import com.momentum.app.models.entity.security.Usuario;
import com.momentum.app.service.security.LoginService;

@RestController
@RequestMapping(value = "/usuario/authenticate")
public class LoginController {
	
	@Autowired
	private LoginService service;
	
	@PostMapping
	private UsuarioModel login(@RequestBody Usuario usuario) {
		return this.service.login(usuario);
	}

}

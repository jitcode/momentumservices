package com.momentum.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.momentum.app.models.entity.TipoEvento;
import com.momentum.app.service.TipoEventoService;

@RestController
@RequestMapping(value = "/tipoevento")
public class TipoEventoController {
	
	@Autowired
	private TipoEventoService service;
	
	@PostMapping
	private TipoEvento save(@RequestBody TipoEvento tipoEvento) {
		return this.service.save(tipoEvento);
	}
	
	@PutMapping("/{id}")
	private TipoEvento actualizar(@PathVariable("id") Long id, @RequestBody TipoEvento tipoEvento) {
		return this.service.save(tipoEvento);
	}
	
	@GetMapping
	private List<TipoEvento> findAll() {
		return this.service.findAll();
	}
	
	@DeleteMapping("/{id}")
	private TipoEvento eliminar(@PathVariable("id") Long id) {
		return this.service.eliminar(id);
	}

}

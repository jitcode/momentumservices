package com.momentum.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.momentum.app.models.entity.Participante;
import com.momentum.app.service.ParticipanteService;

@RestController
@RequestMapping(value = "/participantes")
public class ParticipantesController {

	@Autowired
	private ParticipanteService service;
	
	@PostMapping
	public Participante save(@RequestBody Participante participante) {
		return this.service.save(participante);
	}
	
	@GetMapping
	public List<Participante> findAll(){
		return this.service.findAll();
	}
	
	@GetMapping("participanteEnrolador/{id}")
	public List<Participante> findByParticipanteEnrolador(@PathVariable("id") Long idParticipante){
		return this.service.findByParticipanteEnrolador(idParticipante);
	}
	
	@PutMapping("/{id}")
	public Participante actualizar(@PathVariable("id") Long idParticipante, @RequestBody Participante participante) {
		return this.service.actualizar(idParticipante, participante);
	}
	
	@GetMapping("/{id}")
	public Participante findById(@PathVariable("id") Long idParticipante) {
		return this.service.findById(idParticipante);
	}
	
	@GetMapping("/search/{nombres}")
	public List<Participante> findById(@PathVariable("nombres") String nombres) {
		return this.service.findByNombres(nombres);
	}
	
	@DeleteMapping("/{id}")
	public void eliminarById(@PathVariable("id") Long idParticipante) {
		this.service.deleteById(idParticipante);
	}
	
}
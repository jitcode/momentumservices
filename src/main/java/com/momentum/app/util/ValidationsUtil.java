package com.momentum.app.util;

import java.util.Iterator;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.hibernate.validator.internal.engine.path.PathImpl;

public class ValidationsUtil {
	
	public static String getConstraintMessage(ConstraintViolationException ex) {
		StringBuilder stringBuilder = new StringBuilder();
		
		Set<ConstraintViolation<?>> set =  ex.getConstraintViolations();
	    for (Iterator<ConstraintViolation<?>> iterator = set.iterator();iterator.hasNext(); ) {
	        ConstraintViolation<?> next =  iterator.next();
	        stringBuilder.append(((PathImpl)next.getPropertyPath())
	                .getLeafNode().getName().toUpperCase() + " - " +next.getMessage());


	    }
	    
	    return stringBuilder.toString();
	}

}

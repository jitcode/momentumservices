package com.momentum.app.models.dto;

import com.momentum.app.models.entity.security.Rol;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ActualizarUsuarioRequestModel {
	private String correo;
	private Rol rol;
	private String nombres;
	private String apellidoPaterno;
	private String apellidoMaterno;

}

package com.momentum.app.models.dto;

public interface UsuarioChangePassword {
	
	String getPasswordAnterior();
	
	String getPasswordNuevo();
	
	String getConfirmacionPasswordNuevo();

}

package com.momentum.app.models.dto;

import com.momentum.app.models.entity.security.Rol;
import com.momentum.app.models.entity.security.Usuario;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioModel {
	private String correo;
	private String token;
	private Rol rol;
	private String nombre;

	public UsuarioModel(Usuario usuario) {
		this.correo = usuario.getCorreo();
	}

}

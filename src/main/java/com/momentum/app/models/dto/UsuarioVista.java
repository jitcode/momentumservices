package com.momentum.app.models.dto;

public interface UsuarioVista {
	
	String getId();
	
	String getCorreo();
	
	String getNombres();
	
	String getApellidoPaterno();
	
	String getApellidoMaterno();
	
	String getEstatus();

}

package com.momentum.app.models.entity.security;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "usuario")
public class Usuario  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Email
	@Column(name = "correo")
	private String correo;
	
	@Column(name = "nombres")
	private String nombres;
	
	@Column(name = "apellido_paterno")
	private String apellidoPaterno;
	
	@Column(name = "apellido_materno")
	private String apellidoMaterno;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "salt")
	private String salt;
	
	@Column(name = "intentos_fallidos")
	private Integer intentosFallidos;
	
	@Column(name = "fecha_actualizacion_password")
	private Date fechaActualizacionPassword;
	
	@Column(name = "fecha_ultimo_login")
	private Date fechaUltimoLogin;
	
	@Column(name = "fecha_alta")
	private Date fechaAlta;
	
	@Column(name = "estatus")
	private String estatus;
	
	@ManyToOne
	@JoinColumn(name = "rol_id")
	private Rol rol;
	
}

package com.momentum.app.models.entity.security;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "rol")
public class Rol  {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "clave")
	private String clave;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	@ManyToMany
	@JoinTable(name="rol_menu",
			joinColumns= @JoinColumn(name="rol_id", table = "rol", foreignKey = @ForeignKey(name = "rol_id_fkey"), referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name="menu_id", table = "menu", foreignKey = @ForeignKey(name = "menu_id_fkey"), referencedColumnName = "id"))
	private List<Menu> menus;
	
}

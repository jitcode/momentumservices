package com.momentum.app.models.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "participante")
public class Participante {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "participante_enrolador_id")
	private Participante participanteEnrolador;
	
	@Column(name = "nombres")
	private String nombres;
	
	@Column(name = "apellido_paterno")
	private String apellidoPaterno;
	
	@Column(name = "apellido_materno")
	private String apellidoMaterno;
	
	@Email
	@Column(name = "correo")
	private String correo;
	
	@Column(name = "numero_telefono")
	private String numeroTelefono;
	
	@Column(name = "fecha_alta")
	private Date fechaAlta;
	
	public String getNombreCompleto() {
		return this.nombres.trim() + " " + this.apellidoPaterno.trim() + " " + this.apellidoMaterno.trim();
	}
}

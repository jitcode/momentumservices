package com.momentum.app.models.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "participante_evento")
public class ParticipanteEvento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "participante_id")
	private Participante participante;
	
	@ManyToOne
	@JoinColumn(name = "evento_id")
	private Evento evento;
	
	@Column(name = "costo")
	private Double costo;
	
	@Column(name = "asistio")
	private Boolean asistio;

	@Column(name = "graduado")
	private Boolean graduado;
}

insert into Rol(clave, descripcion)
SELECT 'admin', 'Administrador';

insert into Usuario(APELLIDO_MATERNO, APELLIDO_PATERNO, CORREO, 
    ESTATUS, FECHA_ACTUALIZACION_PASSWORD, FECHA_ALTA, 
    FECHA_ULTIMO_LOGIN, INTENTOS_FALLIDOS, NOMBRES, 
    PASSWORD, SALT, ROL_ID)
SELECT
    'Treviño', 
    'Cantu',
    'admin@mail.com',
    'Activo',
    '2020-03-04 11:01:57.331',
    '2020-03-04 11:01:57.331',
    null,
    0,
    'David Alejandro',
    'IPS/BdE0BJ/iNA2R5m9+jtel1jBsW45l9dZ0gQaLVd8=',
    'MDJsIoNaBumaiQnXgIsLfiUXkuqDDw',
    1; 

insert into tipo_movimiento(clave, descripcion)
select 'abono','Abono';

insert into tipo_movimiento(clave, descripcion)
select 'cargo','Cargo';